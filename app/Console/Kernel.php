<?php

namespace App\Console;

use App\Console\Commands\Orders\CancelExpiredOrdersCommand;
use App\Console\Commands\Orders\CheckOrdersPaymentStatusCommand;
use Ensi\LaravelInitialEventPropagation\SetInitialEventArtisanMiddleware;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    public function bootstrap(): void
    {
        parent::bootstrap();
        (new SetInitialEventArtisanMiddleware())->handle();
    }

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(CancelExpiredOrdersCommand::class)->everyMinute();
        $schedule->command(CheckOrdersPaymentStatusCommand::class)->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');
    }
}
