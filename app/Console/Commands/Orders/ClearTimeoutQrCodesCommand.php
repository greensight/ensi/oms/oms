<?php

namespace App\Console\Commands\Orders;

use App\Domain\Orders\Actions\Payment\PaymentQr\DeleteQrCodeAction;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

class ClearTimeoutQrCodesCommand extends Command
{
    protected $signature = 'orders:clear-qr-codes';
    protected $description = 'Удаление файлов с QR-кодом на оплату у заказов, срок оплаты которых больше суток';

    public const SIZE_CHUNK = 100;

    public function handle(DeleteQrCodeAction $action): int
    {
        $timeNow = Date::now()->subDay();

        Order::query()
            ->where('payment_system', PaymentSystemEnum::SBP)
            ->where(function ($query) use ($timeNow) {
                $query->orWhere('created_at', '<', $timeNow);
                $query->orWhereNot('payment_status', PaymentStatusEnum::NOT_PAID);
            })
            ->chunkById(self::SIZE_CHUNK, fn (Collection $chunk) => $this->processChunk($chunk, $action));

        return self::SUCCESS;
    }

    private function processChunk(Collection $orders, DeleteQrCodeAction $action): void
    {
        /** @var Order $order */
        foreach ($orders as $order) {
            try {
                $action->execute($order);
            } catch (\Exception $e) {
                Log::error("Not deleted qr-code for order with ID {$order->id}", [$e->getMessage()]);
            }
        }
    }
}
