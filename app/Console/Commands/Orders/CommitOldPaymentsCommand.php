<?php

namespace App\Console\Commands\Orders;

use App\Domain\Orders\Actions\Payment\CommitOldPaymentsAction;
use Illuminate\Console\Command;

class CommitOldPaymentsCommand extends Command
{
    protected $signature = 'orders:commit-old-payments';
    protected $description = 'Commit held money when payment reach 3 day age';

    public function handle(CommitOldPaymentsAction $action): void
    {
        $action->execute();
    }
}
