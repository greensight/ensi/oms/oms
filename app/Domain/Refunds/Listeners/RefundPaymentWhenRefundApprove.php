<?php

namespace App\Domain\Refunds\Listeners;

use App\Domain\Refunds\Actions\Payment\PaymentSystem\RefundPaymentAction;
use App\Domain\Refunds\Data\RefundStatus;
use App\Domain\Refunds\Events\RefundStatusSaving;

class RefundPaymentWhenRefundApprove
{
    public function __construct(protected RefundPaymentAction $refundPaymentAction)
    {
    }

    public function handle(RefundStatusSaving $event): void
    {
        if (!in_array($event->refund->status, RefundStatus::approved())) {
            return;
        }

        $this->refundPaymentAction->execute($event->refund);
    }
}
