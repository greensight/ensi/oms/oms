<?php

namespace App\Domain\Refunds\Actions\Refund;

use App\Domain\Refunds\Events\RefundStatusSaving;
use App\Domain\Refunds\Models\Refund;

class PatchRefundAction
{
    public function execute(int $id, array $fields): Refund
    {
        /** @var Refund $refund */
        $refund = Refund::query()->findOrFail($id);

        $refund->update($fields);

        if ($refund->wasChanged('status')) {
            RefundStatusSaving::dispatch($refund);
        }

        return $refund;
    }
}
