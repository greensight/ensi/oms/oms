<?php

namespace App\Domain\Refunds\Events;

use App\Domain\Refunds\Models\Refund;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RefundStatusSaving
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(public Refund $refund)
    {
    }
}
