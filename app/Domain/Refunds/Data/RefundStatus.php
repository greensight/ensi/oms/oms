<?php

namespace App\Domain\Refunds\Data;

use App\Domain\Refunds\Models\Refund;
use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;

class RefundStatus
{
    public string $name;

    public function __construct(public RefundStatusEnum $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById(): void
    {
        $this->name = match ($this->id) {
            RefundStatusEnum::NEW => 'Новая',
            RefundStatusEnum::APPROVED => 'Подтверждена',
            RefundStatusEnum::REJECTED => 'Отклонена',
            RefundStatusEnum::CANCELED => 'Отменена',
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];

        foreach (RefundStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }

    public static function cancelled(): array
    {
        return [
            RefundStatusEnum::REJECTED,
            RefundStatusEnum::CANCELED,
        ];
    }

    public static function updatable(): array
    {
        return [
            RefundStatusEnum::NEW,
        ];
    }

    public static function approved(): array
    {
        return [
            RefundStatusEnum::APPROVED,
        ];
    }

    public static function getAllowedNext(Refund $refund): array
    {
        return match ($refund->status) {
            RefundStatusEnum::NEW => [RefundStatusEnum::APPROVED, RefundStatusEnum::REJECTED],
            RefundStatusEnum::APPROVED => [RefundStatusEnum::CANCELED],
            default => [],
        };
    }
}
