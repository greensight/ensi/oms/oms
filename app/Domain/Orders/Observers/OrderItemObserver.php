<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Orders\Actions\Shipment\CalcShipmentAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentCostAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentSizesAction;
use App\Domain\Orders\Models\OrderItem;
use App\Domain\Orders\Support\CalcPrice;

class OrderItemObserver
{
    public function __construct(
        protected CalcShipmentSizesAction $calcShipmentSizesAction,
        protected CalcShipmentCostAction $calcShipmentCostAction,
        protected CalcShipmentAction $calcShipmentAction,
    ) {
    }

    public function saving(OrderItem $orderItem): void
    {
        $this->setPrice($orderItem);
    }

    /**
     * Установить цену элемента корзины со скидкой
     *
     * @param OrderItem $orderItem
     */
    protected function setPrice(OrderItem $orderItem): void
    {
        if ($orderItem->isDirty('price_per_one') || $orderItem->isDirty('qty') || $orderItem->isDirty('is_deleted')) {
            $orderItem->price = CalcPrice::itemPrice($orderItem->getRealQty(), $orderItem->price_per_one);
            $orderItem->cost = CalcPrice::itemPrice($orderItem->getRealQty(), $orderItem->cost_per_one);
        }
    }

    //    public function created(OrderItem $shipmentItem)
    //    {
    //        // todo вынести в экшн изменения состава заказа
    //        if (!Order::$isCreating) {
    //            $this->calcShipmentAction->execute($shipmentItem->shipment);
    //        }
    //    }
    //
    //    public function updated(OrderItem $orderItem)
    //    {
    //        // todo вынести в экшн изменения состава заказа
    //        if (!Order::$isCreating) {
    //            if ($orderItem->isDirty('qty')) {
    //                $this->calcShipmentSizesAction->execute($orderItem->shipment);
    //            }
    //
    //            // todo вынести в экшн изменения состава заказа
    //            if ($orderItem->isDirty('qty') || $orderItem->isDirty('price')) {
    //                $this->calcShipmentCostAction->execute($orderItem->shipment);
    //            }
    //        }
    //    }
}
