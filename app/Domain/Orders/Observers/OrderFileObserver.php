<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Orders\Models\OrderFile;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Storage;

class OrderFileObserver
{
    public function __construct(protected EnsiFilesystemManager $filesystemManager)
    {
    }

    public function deleted(OrderFile $orderFile): void
    {
        Storage::disk($this->filesystemManager->protectedDiskName())->delete($orderFile->path);
    }
}
