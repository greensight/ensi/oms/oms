<?php

namespace App\Domain\Orders\Observers;

use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PaymentSystem;

class OrderObserver
{
    public function __construct(
        protected SendOrderEventAction $sendOrderEventAction,
    ) {
    }

    public function updated(Order $order): void
    {
        $this->sendOrderEventAction->execute($order, ModelEventMessage::UPDATE);
    }

    public function saving(Order $order): void
    {
        $this->setPaymentStatusAt($order);
        $this->setPaymentMethod($order);
        $this->setProblemAt($order);
        $this->setExpiredAt($order);
        $this->setReturnAt($order);
        $this->setPartialReturnAt($order);

        //Данная команда должна быть в самом низу перед всеми $this->set*Status()
        $this->setStatusAt($order);
    }

    /**
     * Установить дату изменения статуса заказа.
     * @param Order $order
     */
    protected function setStatusAt(Order $order): void
    {
        if ($order->isDirty('status')) {
            $order->status_at = now();
        }
    }

    /**
     * Установить дату изменения статуса оплаты заказа.
     * @param Order $order
     */
    protected function setPaymentStatusAt(Order $order): void
    {
        if ($order->isDirty('payment_status')) {
            $order->payment_status_at = now();
        }
    }

    /**
     * Установить метод оплаты
     * @param Order $order
     */
    protected function setPaymentMethod(Order $order): void
    {
        if ($order->isDirty('payment_system')) {
            $order->payment_method = PaymentSystem::getMethodById($order->payment_system);
        }
    }

    /**
     * Установить дату установки флага проблемного заказа
     * @param Order $order
     */
    protected function setProblemAt(Order $order): void
    {
        if ($order->isDirty('is_problem') && $order->is_problem) {
            $order->is_problem_at = now();
        }
    }

    /**
     * Установить дату установки флага просроченного заказа
     * @param Order $order
     */
    protected function setExpiredAt(Order $order): void
    {
        if ($order->isDirty('is_expired') && $order->is_expired) {
            $order->is_expired_at = now();
        }
    }

    /**
     * Установить дату установки флага возвращенного заказа
     * @param Order $order
     */
    protected function setReturnAt(Order $order): void
    {
        if ($order->isDirty('is_return') && $order->is_return) {
            $order->is_return_at = now();
        }
    }

    /**
     * Установить дату установки флага частично возвращенного заказа
     * @param Order $order
     */
    protected function setPartialReturnAt(Order $order): void
    {
        if ($order->isDirty('is_partial_return') && $order->is_partial_return) {
            $order->is_partial_return_at = now();
        }
    }
}
