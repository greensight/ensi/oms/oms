<?php

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

test("Order status update success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::WAIT_PAY]);
    $newStatus = OrderStatus::getAllowedNext($order)[0];
    $order->status = $newStatus;
    assertEquals($order->status, $newStatus);
})->with(FakerProvider::$optionalDataset);

test("Order status update bad", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create(['status' => OrderStatusEnum::WAIT_PAY]);
    $statuses = array_column(OrderStatusEnum::cases(), 'value');
    $allowed = array_map(fn (OrderStatusEnum $status) => $status->value, OrderStatus::getAllowedNext($order));
    $badStatuses = array_diff($statuses, $allowed);
    $order->status = OrderStatusEnum::from(end($badStatuses));
})->with(FakerProvider::$optionalDataset)
    ->throws(ValidateException::class);

test("Order payment status update success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create(['payment_status' => PaymentStatusEnum::NOT_PAID]);
    $newStatus = PaymentStatus::getAllowedNext($order)[0];
    $order->payment_status = $newStatus;
    assertEquals($order->payment_status, $newStatus);
})->with(FakerProvider::$optionalDataset);

test("Order payment status update bad", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Order $order */
    $order = Order::factory()->create(['payment_status' => PaymentStatusEnum::NOT_PAID]);
    $statuses = array_column(PaymentStatusEnum::cases(), 'value');
    $allowed = array_map(fn (PaymentStatusEnum $status) => $status->value, PaymentStatus::getAllowedNext($order));
    $badStatuses = array_diff($statuses, $allowed);
    $order->payment_status = PaymentStatusEnum::from(end($badStatuses));
})->with(FakerProvider::$optionalDataset)
    ->throws(ValidateException::class);
