<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Models\Order;
use App\Domain\Orders\Models\OrderFile;
use Ensi\LaravelTestFactories\BaseModelFactory;

class OrderFileFactory extends BaseModelFactory
{
    protected $model = OrderFile::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'path' => 'order_files/file.png',
            'original_name' => 'file.png',
        ];
    }

    public function withPath(string $path)
    {
        return $this->state(fn (array $attributes) => [
            'path' => $path,
            'original_name' => pathinfo($path, PATHINFO_FILENAME),
        ]);
    }
}
