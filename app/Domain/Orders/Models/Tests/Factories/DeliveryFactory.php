<?php

namespace App\Domain\Orders\Models\Tests\Factories;

use App\Domain\Orders\Data\DeliveryStatus;
use App\Domain\Orders\Data\Timeslot;
use App\Domain\Orders\Models\Delivery;
use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeliveryFactory extends BaseModelFactory
{
    protected $model = Delivery::class;

    public function definition()
    {
        return [
            'order_id' => Order::factory(),
            'status' => $this->faker->randomEnum(DeliveryStatusEnum::cases()),
            'number' => $this->faker->unique()->numerify('######-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'date' => $this->faker->date(),
            'timeslot' => $this->faker->boolean() ? Timeslot::factory()->make() : null,
        ];
    }

    public function done(): self
    {
        return $this->state([
            'status' => $this->faker->randomElement(DeliveryStatus::done()),
        ]);
    }

    public function forApprovedOrder(?Order $order = null): self
    {
        $order = $order ?: Order::factory()->create(['status' => OrderStatusEnum::APPROVED]);

        return $this->for($order);
    }
}
