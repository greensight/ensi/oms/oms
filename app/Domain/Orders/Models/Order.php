<?php

namespace App\Domain\Orders\Models;

use App\Domain\Orders\Data\DeliveryAddress;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Data\PaymentStatus;
use App\Domain\Orders\Models\Tests\Factories\OrderFactory;
use App\Domain\PaymentSystems\Data\PaymentSystem as PaymentSystemEnumInfo;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Domain\Refunds\Models\Refund;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderSourceEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\OrderStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Carbon\CarbonInterface;
use Ensi\LaravelAuditing\Contracts\Auditable;
use Ensi\LaravelAuditing\SupportsAudit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id id заказа
 * @property string $number номер заказа
 * @property int|null $responsible_id id ответственного за заказ
 * @property int $customer_id id покупателя
 * @property string $customer_email почта покупателя
 * @property int $cost стоимость (без учета скидки) (рассчитывается автоматически) (коп.)
 * @property int $price стоимость (с учетом скидок) (рассчитывается автоматически) (коп.)
 * @property int $delivery_service служба доставки
 * @property int $delivery_method метод доставки
 * @property int $delivery_cost стоимость доставки (без учета скидки) (коп.)
 * @property int $delivery_price стоимость доставки (с учетом скидки) (коп.)
 * @property int $delivery_point_id - идентификатор пункта самовывоза из сервиса логистики
 * @property DeliveryAddress|null $delivery_address - адрес доставки
 * @property string|null $delivery_comment комментарий к доставке
 * @property string|null $receiver_name - имя получателя
 * @property string|null $receiver_phone - телефон получателя
 * @property string|null $receiver_email - e-mail получателя
 * @property int $spent_bonus списано бонусов
 * @property int $added_bonus начислено бонусов
 * @property string|null $promo_code использованный промокод
 * @property OrderStatusEnum $status статус
 * @property CarbonInterface|null $status_at дата установки статуса заказа
 * @property PaymentStatusEnum $payment_status статус оплаты
 * @property CarbonInterface|null $payment_status_at дата установки статуса оплаты
 * @property CarbonInterface|null $payed_at дата оплаты
 * @property int|null $payed_price сумма оплаты (захолдировано или оплачено)
 * @property PaymentSystemEnum $payment_system система оплаты
 * @property PaymentMethodEnum $payment_method метод оплаты
 * @property CarbonInterface|null $payment_expires_at дата, когда оплата станет просрочена
 * @property array $payment_data данные по оплате, зависят от выбранной системы оплаты
 * @property string|null $payment_link ссылка на оплату во внешней системе
 * @property string|null $payment_external_id id оплаты во внешней системе
 * @property bool $is_changed флаг, что заказ был изменён
 * @property bool $is_expired флаг, что заказ просроченный
 * @property CarbonInterface|null $is_expired_at дата установки флага просроченного заказа
 * @property bool $is_return флаг, что заказ возвращен
 * @property CarbonInterface|null $is_return_at дата установки флага возвращенного заказа
 * @property bool $is_partial_return флаг, что заказ частично возвращен
 * @property CarbonInterface|null $is_partial_return_at дата установки флага частично возвращенного заказа
 * @property bool $is_problem флаг, что заказ проблемный
 * @property CarbonInterface|null $is_problem_at дата установки флага проблемного заказа
 * @property string $problem_comment последнее сообщение продавца о проблеме со сборкой
 * @property OrderSourceEnum $source источник взаимодействия
 * @property string|null $client_comment комментарий клиента
 * @property CarbonInterface|null $created_at дата создание
 * @property CarbonInterface|null $updated_at дата обновления
 * @property Collection|OrderItem[] $items корзина
 * @property Collection|Delivery[] $deliveries отправления заказа
 * @property Collection|OrderComment[] $comments Комментарии к заказу
 * @property Collection|OrderFile[] $files Вложения к заказу
 * @property Collection|Refund[] $refunds Возвраты заказа
 * @method static Builder|static whereCustomerId(int $value)
 */
class Order extends Model implements Auditable
{
    use SupportsAudit;

    /** @var bool Флаг, который говорит нам что код находится в процессе создания заказа.
     * Нужен для того чтобы отключить некоторые действия в событиях.
     * Эти действия потом вручную реализуются в классе создания заказа
     * Пример: при создании ShipmentItem идёт пересчет веса/цены в Shipment. Это не нужно делать, пока не создадутся все ShipmentItems
     */
    public static $isCreating = false;

    protected $table = 'orders';

    protected $fillable = [
        'responsible_id',
        'status',
        'delivery_service',
        'delivery_method',
        'delivery_point_id',
        'delivery_address',
        'delivery_price',
        'delivery_cost',
        'delivery_comment',
        'client_comment',
        'receiver_name',
        'receiver_phone',
        'receiver_email',
        'is_problem',
        'problem_comment',
    ];

    protected $casts = [
        'payment_data' => 'array',
        'is_return' => 'boolean',
        'is_partial_return' => 'boolean',
        'is_problem' => 'boolean',
        'source' => OrderSourceEnum::class,
        'payment_method' => PaymentMethodEnum::class,
        'payment_system' => PaymentSystemEnum::class,
        'status' => OrderStatusEnum::class,
        'payment_status' => PaymentStatusEnum::class,

        /** DateTime */
        'payment_status_at' => 'datetime',
        'is_problem_at' => 'datetime',
        'is_expired_at' => 'datetime',
        'is_return_at' => 'datetime',
        'is_partial_return_at' => 'datetime',
        'status_at' => 'datetime',
        'payed_at' => 'datetime',
        'payment_expires_at' => 'datetime',
    ];

    protected $hidden = ['payment_data'];

    public function __construct(array $attributes = [])
    {
        $this->payment_data = [];
        parent::__construct($attributes);
    }

    public static function factory(): OrderFactory
    {
        return OrderFactory::new();
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function deliveries(): HasMany
    {
        return $this->hasMany(Delivery::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(OrderComment::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(OrderFile::class);
    }

    public function refunds(): HasMany
    {
        return $this->hasMany(Refund::class);
    }

    public function paymentSystem(): PaymentSystem
    {
        return PaymentSystem::getById($this->payment_system);
    }

    /**
     * Заказ оплачен?
     * @return bool
     */
    public function isPaid(): bool
    {
        return PaymentStatus::isPaid($this->payment_status);
    }

    public function scopeManagerCommentLike(Builder $query, $comment): Builder
    {
        return $query->whereHas('comments', function (Builder $query) use ($comment) {
            $query->where('text', 'ilike', "%{$comment}%");
        });
    }

    public function getPaymentQrImageName(): string
    {
        $paymentData = $this->payment_data;

        return $paymentData['qrCode'] ?? "";
    }

    protected function getDeliveryAddressAttribute(): ?DeliveryAddress
    {
        return !is_null($this->attributes['delivery_address'] ?? null)
            ? new DeliveryAddress(json_decode($this->attributes['delivery_address']))
            : null;
    }

    protected function setDeliveryAddressAttribute(?DeliveryAddress $address): void
    {
        $this->attributes['delivery_address'] = $address?->toJson();
    }

    protected function setStatusAttribute(OrderStatusEnum|int $value): void
    {
        $status = is_int($value) ? OrderStatusEnum::from($value) : $value;

        $currentStatus = $this->attributes['status'] ?? null;
        if ($currentStatus && $this->status != $status && !in_array($status, OrderStatus::getAllowedNext($this))) {
            $newStatus = new OrderStatus($status);
            $oldStatus = new OrderStatus($this->status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['status'] = $status->value;
    }

    protected function setPaymentStatusAttribute(PaymentStatusEnum|int $value): void
    {
        $paymentStatus = is_int($value) ? PaymentStatusEnum::from($value) : $value;
        $currentStatus = $this->attributes['payment_status'] ?? null;
        if ($currentStatus && $this->payment_status != $paymentStatus && !in_array($paymentStatus, PaymentStatus::getAllowedNext($this))) {
            $newStatus = new PaymentStatus($paymentStatus);
            $oldStatus = new PaymentStatus($this->payment_status);

            throw new ValidateException("Статус \"{$newStatus->name}\" нельзя установить из статуса \"{$oldStatus->name}\"");
        }
        $this->attributes['payment_status'] = $paymentStatus->value;
    }

    public static function makePaymentNumber(int $orderId): string
    {
        return config('common.order_number_prefix') . '_' . $orderId;
    }

    /**
     * Разрешаем ли редактировать заказ в админке
     * @return bool
     */
    public function isEditable(): bool
    {
        return in_array($this->status, OrderStatus::editable())
            && ($this->payment_method == PaymentMethodEnum::OFFLINE || in_array($this->payment_system, PaymentSystemEnumInfo::editable()));
    }

    public function scopeWhereIsEditable(Builder $query): Builder
    {
        return $query->whereIn('status', OrderStatus::editable())
            ->where(
                fn (Builder $orBuilder) => $orBuilder
                    ->orWhere('payment_method', PaymentMethodEnum::OFFLINE)
                    ->orWhereIn('payment_system', PaymentSystemEnumInfo::editable())
            );
    }

    /**
     * @return Collection<OrderItem>
     */
    public function getRealItems(): Collection
    {
        return $this->items->filter(fn (OrderItem $i) => $i->getRealQty() > 0);
    }
}
