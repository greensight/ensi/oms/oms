<?php

namespace App\Domain\Orders\Models\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Stock;

class StockFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'store_id' => $this->faker->unique()->modelId(),
            'product_id' => $this->faker->unique()->modelId(),
            'offer_id' => $this->faker->unique()->modelId(),
            'qty' => $this->faker->randomFloat(0, 1, 99),
        ];
    }

    public function make(array $extra = []): Stock
    {
        return new Stock($this->makeArray($extra));
    }
}
