<?php

namespace App\Domain\Orders\Models\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\SearchProductsResponse;

class ProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->text,
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'status_id' => $this->faker->modelId(),
            'allow_publish' => true,

            'external_id' => $this->faker->unique()->numerify('######'),
            'barcode' => $this->faker->ean13(),
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->randomFloat(3, 1, 100),
            'length' => $this->faker->randomFloat(2, 1, 1000),
            'height' => $this->faker->randomFloat(2, 1, 1000),
            'width' => $this->faker->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseSearch(
        array $extras = [],
        int $count = 1,
        mixed $pagination = null,
        ?callable $beforeCallback = null
    ): SearchProductsResponse {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extras, $count, $pagination, $beforeCallback);
    }
}
