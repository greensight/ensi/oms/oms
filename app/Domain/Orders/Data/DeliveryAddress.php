<?php

namespace App\Domain\Orders\Data;

use App\Domain\Orders\Data\Tests\Factories\DeliveryAddressFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CountryCodeEnum;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;
use Illuminate\Validation\Rules\Enum;

/**
 * Class DeliveryAddress
 * @package App\Domain\Orders\Data
 * @property string $address_string - Адрес полностью
 * @property string $country_code - Код страны
 * @property string $post_index - Почтовые индекс
 * @property string $region - Регион
 * @property string $region_guid - GUID региона
 * @property string|null $area - Область
 * @property string|null $area_guid - GUID области
 * @property string|null $city - Город
 * @property string|null $city_guid - Код города
 * @property string|null $street - Улица
 * @property string|null $house - Дом
 * @property string|null $block - Корпус
 * @property string|null $flat - Квартира
 * @property string|null $floor - Этаж
 * @property string|null $porch - Подъезд
 * @property string|null $intercom - Домофон
 * @property string $geo_lat - Широта
 * @property string $geo_lon - Долгота
 */
class DeliveryAddress extends Fluent
{
    public static function rules(): array
    {
        return [
            "address_string" => ['required', 'string'],
            "country_code" => ['required', 'string', new Enum(CountryCodeEnum::class)],
            "post_index" => ['required', 'string'],
            "region" => ['required', 'string'],
            "region_guid" => ['required', 'string'],
            "area" => ['nullable', 'string'],
            "area_guid" => ['nullable', 'string'],
            "city" => ['nullable', 'string'],
            "city_guid" => ['nullable', 'string'],
            "street" => ['nullable', 'string'],
            "house" => ['nullable', 'string'],
            "block" => ['nullable', 'string'],
            "flat" => ['nullable', 'string'],
            "floor" => ['nullable', 'string'],
            "porch" => ['nullable', 'string'],
            "intercom" => ['nullable', 'string'],
            "geo_lat" => ['required', 'string'],
            "geo_lon" => ['required', 'string'],
        ];
    }

    public static function validateOrFail(array $deliveryAddress): self
    {
        $validator = Validator::make($deliveryAddress, self::rules());

        return new self($validator->validate());
    }

    /**
     * Формирует строку с полным адресом.
     *
     * @param bool $addNewLine Добавить перенос строки перед домофоном (если он задан)
     * @return string
     */
    public function getFullString(bool $addNewLine = false): string
    {
        $addressParts = [
            $this->address_string ?? '',
            $this->porch ? 'под. ' . $this->porch : null,
            $this->floor ? 'эт. ' . $this->floor : null,
            $this->flat ? 'кв. ' . $this->flat : null,
        ];

        $intercomLine = $this->intercom ? 'домофон ' . $this->intercom : null;
        if ($intercomLine) {
            $intercomLine = ($addNewLine ? "\n" : '') . $intercomLine;
        }
        $addressParts[] = $intercomLine;

        return join(', ', array_filter($addressParts));
    }

    public static function factory(): DeliveryAddressFactory
    {
        return DeliveryAddressFactory::new();
    }
}
