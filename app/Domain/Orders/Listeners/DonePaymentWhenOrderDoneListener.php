<?php

namespace App\Domain\Orders\Listeners;

use App\Domain\Orders\Actions\Payment\PaymentSystem\CommitPaymentAction;
use App\Domain\Orders\Actions\Payment\PaymentSystem\RevertPaymentAction;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Events\OrderStatusUpdated;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;

class DonePaymentWhenOrderDoneListener
{
    public function __construct(
        protected CommitPaymentAction $commitPaymentAction,
        protected RevertPaymentAction $revertPaymentAction,
    ) {
    }

    public function handle(OrderStatusUpdated $event): void
    {
        $order = $event->order;

        // Если оплата не онлайн или заказ не оплачен, то ничего не делаем
        if ($order->payment_method != PaymentMethodEnum::ONLINE || !$order->isPaid()) {
            return;
        }

        // Если заказ завершился успешно, то коммитим оплату или делаем возврат если сумма 0. Если заказ отменен, то делаем возврат
        if (in_array($order->status, OrderStatus::done())) {
            if ($order->price != 0) {
                if ($order->payment_status == PaymentStatusEnum::HOLD) {
                    $this->commitPaymentAction->execute($order);
                }
            } else {
                $this->revertPaymentAction->execute($order);
            }
        }
    }
}
