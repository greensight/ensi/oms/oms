<?php

namespace App\Domain\Orders\Actions\Payment\PaymentSystem;

use App\Domain\Orders\Actions\Payment\MakePaymentDataAction;
use App\Domain\Orders\Actions\Payment\SaveFromPaymentDataAction;
use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Systems\PushPaymentData;

class HandlePaymentAction
{
    public function __construct(
        protected MakePaymentDataAction $makePaymentDataAction,
        protected SaveFromPaymentDataAction $savePaymentAction,
    ) {
    }

    public function execute(PushPaymentData $pushPaymentData): void
    {
        /** @var Order $order */
        $order = Order::query()->where('payment_external_id', $pushPaymentData->externalId)->firstOrFail();

        $paymentData = $this->makePaymentDataAction->execute($order);
        $order->paymentSystem()->handlePushPayment($paymentData, $pushPaymentData);
        $this->savePaymentAction->execute($order, $paymentData);
    }
}
