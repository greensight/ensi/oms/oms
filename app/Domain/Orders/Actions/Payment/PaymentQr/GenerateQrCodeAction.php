<?php

namespace App\Domain\Orders\Actions\Payment\PaymentQr;

use App\Domain\Orders\Data\OrderPaymentQr;
use App\Domain\Orders\Models\Order;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class GenerateQrCodeAction
{
    public function __construct(private readonly EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $orderId, string $paymentLink): string
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);

        $hash = Str::random(20);
        $baseName = OrderPaymentQr::BASE_FILE_NAME;
        $fileName = "{$baseName}_{$order->id}_{$hash}.svg";
        $this->generateQR($paymentLink, $fileName);

        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->protectedDiskName());


        $filePath = OrderPaymentQr::DEFAULT_PATH . "{$fileName}";
        $file = new UploadedFile($filePath, File::basename($filePath));
        $savePath = $disk->putFileAs(OrderPaymentQr::DIR_NAME, $file, $fileName);
        if (!$savePath) {
            throw new RuntimeException("Unable to save file $fileName to directory $filePath");
        }

        return $savePath;
    }

    private function generateQR(string $link, string $fileName): void
    {
        $render = new ImageRenderer(
            new RendererStyle(400),
            new SvgImageBackEnd()
        );
        $writer = new Writer($render);
        $writer->writeFile($link, OrderPaymentQr::DEFAULT_PATH . "{$fileName}");
    }
}
