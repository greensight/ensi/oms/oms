<?php

namespace App\Domain\Orders\Actions\Payment\PaymentQr;

use App\Domain\Orders\Models\Order;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

class DeleteQrCodeAction
{
    public function __construct(private readonly EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(Order $order): void
    {
        $filePath = $order->getPaymentQrImageName();
        if ($filePath == "") {
            return;
        }
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->protectedDiskName());
        $disk->delete($filePath);
    }
}
