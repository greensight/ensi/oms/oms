<?php

namespace App\Domain\Orders\Actions\Payment;

use App\Domain\Orders\Models\Order;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Date;
use Psr\Log\LoggerInterface;

class CancelExpiredPaymentsAction
{
    protected LoggerInterface $logger;

    public function __construct(protected SetStatusPaymentAction $setTimeoutPaymentAction)
    {
        $this->logger = logger()->channel('cancel_expired_payments');
    }

    public function execute(): void
    {
        /** @var Collection<Order> $orders */
        $orders = Order::query()
            ->where('payment_status', PaymentStatusEnum::NOT_PAID)
            ->where('payment_expires_at', '<', Date::now()->format('Y-m-d H:i:s'))
            ->get();
        foreach ($orders as $order) {
            try {
                $this->setTimeoutPaymentAction->execute($order, PaymentStatusEnum::TIMEOUT);
            } catch (Exception $e) {
                $this->logger->error("Error occurred during attempt to cancel expired payment. Order ID {$order->id}", ['message' => $e->getMessage()]);
            }
        }
    }
}
