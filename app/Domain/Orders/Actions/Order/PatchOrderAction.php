<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;

class PatchOrderAction
{
    public function __construct(protected SaveOrderAction $saveOrderAction)
    {
    }

    public function execute(int $orderId, array $fields): Order
    {
        /** @var Order $order */
        $order = Order::query()->findOrFail($orderId);
        if (isset($fields['client_comment']) && !in_array($order->status, OrderStatus::waitProcess())) {
            throw new ValidateException('Client comment cannot be updated in current status');
        }

        $order->fill($fields);

        $this->saveOrderAction->execute($order);

        $order->setRelations([]);

        return $order;
    }
}
