<?php

namespace App\Domain\Orders\Actions\Order;

use App\Domain\Orders\Models\Order;

class CalcOrderPriceAction
{
    public function execute(Order $order): void
    {
        $order->loadMissing('items');

        $price = 0;
        $cost = 0;
        foreach ($order->getRealItems() as $orderItem) {
            $price += $orderItem->price;
            $cost += $orderItem->cost;
        }

        $order->price = $price + $order->delivery_price;
        $order->cost = $cost + $order->delivery_cost;
    }
}
