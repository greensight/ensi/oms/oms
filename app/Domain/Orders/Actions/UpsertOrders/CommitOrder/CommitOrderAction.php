<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder;

use App\Domain\Kafka\Actions\Send\SendDeliveryEventAction;
use App\Domain\Kafka\Actions\Send\SendOrderEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Orders\Actions\Payment\PaymentSystem\StartPaymentAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\CommitContext;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\CommitData;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Stages\CreateItemsAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Stages\CreateOrderAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Stages\ReserveStockAction;
use App\Domain\Orders\Actions\UpsertOrders\Stages\LoadProductInfoAction;
use App\Domain\Orders\Models\Order;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentMethodEnum;
use Ensi\OffersClient\ApiException as OffersApiException;
use Ensi\PimClient\ApiException as PimApiException;
use Exception;
use Illuminate\Support\Facades\DB;

class CommitOrderAction
{
    protected CommitContext $context;

    public function __construct(
        protected LoadProductInfoAction $loadProductInfoAction,
        protected CreateOrderAction $createOrderAction,
        protected CreateItemsAction $createItemsAction,
        protected ReserveStockAction $reserveStockAction,
        protected SendOrderEventAction $sendOrderEventAction,
        protected SendDeliveryEventAction $sendDeliveryEventAction,
        protected StartPaymentAction $startPaymentAction
    ) {
    }

    /**
     * @throws ValidateException
     * @throws OffersApiException
     * @throws PimApiException
     */
    public function execute(CommitData $commitData): Order
    {
        try {
            $this->context = new CommitContext($commitData);

            $this->context->logger->info('Start commit', [
                'commitData' => $commitData,
            ]);

            Order::$isCreating = true;
            DB::transaction(function () {
                $this->loadProductInfoAction->execute($this->context);
                $this->createOrderAction->execute($this->context);
                $this->createItemsAction->execute($this->context);

                $order = $this->context->getOrder();
                if ($order->payment_method != PaymentMethodEnum::OFFLINE) {
                    $returnUrl = toPublicPath(route('successPayedPage', ['orderNumber' => $order->number]));
                    $failUrl = toPublicPath(route('paymentFailedPage', ['orderNumber' => $order->number]));

                    $this->startPaymentAction->execute($order, $returnUrl, $failUrl);
                }
            });
            Order::$isCreating = false;

            $this->sendOrderEventAction->execute($this->context->getOrder(), ModelEventMessage::CREATE);
            foreach ($this->context->getOrder()->deliveries as $delivery) {
                $this->sendDeliveryEventAction->execute($delivery, ModelEventMessage::CREATE);
            }
            $this->reserveStockAction->execute($this->context);

            return $this->context->getOrder();
        } catch (ValidateException $e) {
            $this->context->logger->warning($e->getMessage());

            throw $e;
        } catch (Exception $e) {
            $this->context->logger->error($e->getMessage(), [
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);

            throw $e;
        }
    }
}
