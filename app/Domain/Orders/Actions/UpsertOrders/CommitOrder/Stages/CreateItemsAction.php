<?php

namespace App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Stages;

use App\Domain\Orders\Actions\Shipment\CalcShipmentAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\Data\CommitContext;
use App\Domain\Orders\Actions\UpsertOrders\Stages\MakeOrderItemAction;
use App\Domain\Orders\Actions\UpsertOrders\Stages\MakeShipmentAction;
use App\Domain\Orders\Models\Delivery;
use App\Http\ApiV1\OpenApiGenerated\Enums\DeliveryStatusEnum;
use Illuminate\Database\Eloquent\Collection;

class CreateItemsAction
{
    public function __construct(
        protected CalcShipmentAction $calcShipmentAction,
        protected MakeOrderItemAction $makeOrderItemAction,
        protected MakeShipmentAction $makeShipmentAction,
    ) {
    }

    public function execute(CommitContext $context): void
    {
        $context->logger->info('Start create shipments');

        $order = $context->getOrder();

        $deliveries = $this->getDeliveries($context);
        $order->setRelation('deliveries', $deliveries);
    }

    private function getDeliveries(CommitContext $context): Collection
    {
        $order = $context->getOrder();
        $deliveries = new Collection();
        foreach ($context->data->deliveries as $i => $deliveryData) {
            $delivery = new Delivery();
            $delivery->setRelation('order', $order);
            $delivery->setRelation('shipments', new Collection());
            $delivery->order_id = $order->id;
            $delivery->status = DeliveryStatusEnum::NEW;
            $delivery->number = Delivery::makeNumber($order->number, ++$i);

            $delivery->cost = $deliveryData->cost;

            $delivery->date = $deliveryData->date;
            $delivery->timeslot = $deliveryData->timeslot;

            $delivery->save();
            $deliveries->push($delivery);

            foreach ($deliveryData->shipments as $shipmentData) {
                $shipment = $this->makeShipmentAction->execute($shipmentData->storeId, $delivery);
                $shipment->save();

                foreach ($shipmentData->items as $itemData) {
                    $productInfo = $context->getProductInfo($itemData->offerId);
                    $orderItem = $this->makeOrderItemAction->execute($itemData, $productInfo, $shipment, $order);
                    $orderItem->save();

                    // Заполняем информацию о товарах, которые надо будет зарезервировать
                    $context->addReserveItem($orderItem, $shipmentData->storeId);
                }

                $this->calcShipmentAction->execute($shipment);
            }
        }

        return $deliveries;
    }
}
