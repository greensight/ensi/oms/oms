<?php

namespace App\Domain\Orders\Actions\UpsertOrders\Stages;

use App\Domain\Orders\Actions\UpsertOrders\Data\BaseUpsertOrderContext;
use App\Domain\Orders\Actions\UpsertOrders\Data\ProductInfoData;
use App\Exceptions\ValidateException;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\ApiException as OffersApiException;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\PaginationTypeEnum as OffersPaginationTypeEnum;
use Ensi\OffersClient\Dto\RequestBodyPagination as OffersRequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException as PimApiException;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;

class LoadProductInfoAction
{
    public function __construct(
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
    ) {
    }

    /**
     * @throws ValidateException
     * @throws OffersApiException
     * @throws PimApiException
     */
    public function execute(BaseUpsertOrderContext $context): void
    {
        $context->logger->info('Start fill product info');

        $items = $context->items();
        $offerIds = $items->pluck('offerId')->all();
        $offers = $this->loadOffers($offerIds);
        $productIds = $offers->pluck('product_id')->all();
        $products = $this->loadProducts($productIds);

        if (!$this->checkProductsActive($products)) {
            throw new ValidateException('В запросе присутствует неактивный товар');
        }

        foreach ($items as $item) {
            $offer = $offers->get($item->offerId);
            if (!$offer) {
                throw new ValidateException("Для товара {$item->offerId} не найден оффер");
            }

            if (!$offer->getIsRealActive()) {
                throw new ValidateException("Оффер {$item->offerId} не может быть добавлен в заказ.");
            }

            $product = $products->get($offer->getProductId());
            if (!$product) {
                throw new ValidateException("Для товара {$item->offerId} не найден товар");
            }

            if ($product->getType() === ProductTypeEnum::WEIGHT) {
                if ($product->getOrderMinvol() > $item->qty) {
                    throw new ValidateException(
                        "Для товара {$item->offerId} нельзя установить количество меньше {$product->getOrderMinvol()}"
                    );
                }
            }

            //            TODO: stock скрыто в рамках задачи #90290
            //            /** @var Stock|null $stock */
            //            $stock = collect($offer->getStocks())->first();
            //            if (!$stock) {
            //                throw new ValidateException("Для товара {$item->offerId} не найден сток");
            //            }
            //            if ($stock->getQty() < $item->qty) {
            //                throw new ValidateException("Кол-ва товара {$item->offerId} недостаточно на складе");
            //            }
            $productInfo = new ProductInfoData();
            $productInfo->offer = $offer;
            $productInfo->product = $product;

            $context->setProductInfo($item->offerId, $productInfo);
        }
    }

    /**
     * @param Collection<int,Product> $products
     * @return bool
     */
    protected function checkProductsActive(Collection $products): bool
    {
        return $products->every(fn (Product $product) => $product->getAllowPublish() === true);
    }

    /**
     * @param int[] $offerIds
     * @return Collection<int,Offer>
     * @throws OffersApiException
     */
    protected function loadOffers(array $offerIds): Collection
    {
        $searchOffersRequest = new SearchOffersRequest();
        $searchOffersRequest
            ->setFilter((object)[
                'id' => $offerIds,
            ])
            ->setInclude(['stocks'])
            ->setPagination(
                (new OffersRequestBodyPagination())->setLimit(count($offerIds))
                    ->setType(OffersPaginationTypeEnum::CURSOR)
            );

        return collect($this->offersApi->searchOffers($searchOffersRequest)->getData())->keyBy('id');
    }

    /**
     * @param int[] $productIds
     * @return Collection<int,Product>
     * @throws PimApiException
     */
    protected function loadProducts(array $productIds): Collection
    {
        $searchProductsRequest = new SearchProductsRequest();
        $searchProductsRequest->setFilter((object)[
            'id' => $productIds,
        ])->setPagination(
            (new PimRequestBodyPagination())->setLimit(count($productIds))
                ->setType(PimPaginationTypeEnum::CURSOR)
        );

        return collect($this->productsApi->searchProducts($searchProductsRequest)->getData())->keyBy('id');
    }
}
