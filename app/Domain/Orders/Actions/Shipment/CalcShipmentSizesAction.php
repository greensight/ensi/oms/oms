<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Actions\CalcModelSizesAction;
use App\Domain\Orders\Actions\Delivery\CalcDeliverySizesAction;
use App\Domain\Orders\Models\Shipment;

class CalcShipmentSizesAction extends CalcModelSizesAction
{
    protected Shipment $shipment;

    public function __construct(
        protected CalcDeliverySizesAction $calcDeliverySizesAction
    ) {
    }

    public function execute(Shipment $shipment): void
    {
        $this->shipment = $shipment;
        $this->fillSizes();

        if ($shipment->isDirty(['weight', 'width', 'height', 'length'])) {
            $this->shipment->save();
            $shipment->load('delivery.shipments');
            $this->calcDeliverySizesAction->execute($shipment->delivery);
        }
    }

    protected function calcVolume(): void
    {
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->getRealOrderItems() as $orderItem) {
            $this->volume += $orderItem->product_width * $orderItem->product_height * $orderItem->product_length * $orderItem->getRealQty();
        }
    }

    protected function calcMaxSide(): void
    {
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->getRealOrderItems() as $orderItem) {
            $this->checkMaxSize($orderItem->product_width, static::MAX_WIDTH);
            $this->checkMaxSize($orderItem->product_height, static::MAX_HEIGHT);
            $this->checkMaxSize($orderItem->product_length, static::MAX_LENGTH);
        }
    }

    protected function setWeight(): void
    {
        $weight = 0;
        $this->shipment->loadMissing(['orderItems']);

        foreach ($this->shipment->getRealOrderItems() as $orderItem) {
            $weight += $orderItem->product_weight * $orderItem->getRealQty();
        }

        // TODO from #104941
        $this->shipment->weight = (int)ceil($weight);
    }

    protected function setWidth(int $width): void
    {
        $this->shipment->width = $width;
    }

    protected function setHeight(int $height): void
    {
        $this->shipment->height = $height;
    }

    protected function setLength(int $length): void
    {
        $this->shipment->length = $length;
    }
}
