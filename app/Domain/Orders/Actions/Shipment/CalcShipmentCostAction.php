<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Models\Shipment;

class CalcShipmentCostAction
{
    public function execute(Shipment $shipment): void
    {
        $cost = 0;
        $shipment->loadMissing('orderItems');

        foreach ($shipment->getRealOrderItems() as $orderItem) {
            $cost += $orderItem->price;
        }

        $shipment->cost = $cost;
        $shipment->save();
    }
}
