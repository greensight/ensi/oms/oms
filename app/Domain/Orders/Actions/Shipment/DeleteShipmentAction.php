<?php

namespace App\Domain\Orders\Actions\Shipment;

use App\Domain\Orders\Actions\Delivery\CalcDeliverySizesAction;
use App\Domain\Orders\Models\Shipment;

class DeleteShipmentAction
{
    public function __construct(
        protected CalcDeliverySizesAction $calcDeliverySizesAction
    ) {
    }

    public function execute(Shipment $shipment): void
    {
        $shipment->delete();
        $this->calcDeliverySizesAction->execute($shipment->delivery);
    }
}
