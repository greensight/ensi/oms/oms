<?php

namespace App\Domain\Orders\Actions\OrderItem;

use App\Domain\Orders\Actions\Order\CalcOrderPriceAction;
use App\Domain\Orders\Actions\Order\LoadForEditOrderAction;
use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Actions\Shipment\CalcShipmentsAction;
use App\Domain\Orders\Models\OrderItem;
use App\Exceptions\ValidateException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DeleteOrderItemsAction
{
    public function __construct(
        protected CalcShipmentsAction $calcShipmentsAction,
        protected LoadForEditOrderAction $loadOrderAction,
        protected CalcOrderPriceAction $calcOrderPriceAction,
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    /**
     * @throws ValidateException
     */
    public function execute(int $orderId, array $offerIds): void
    {
        $order = $this->loadOrderAction->execute($orderId);

        /** @var Collection|OrderItem[] $orderItems */
        $orderItems = OrderItem::query()
            ->whereIn('offer_id', $offerIds)
            ->where('order_id', $order->id)
            ->where('is_deleted', false)
            ->with('shipment')
            ->get();

        if (count($offerIds) != $orderItems->count()) {
            throw new ValidateException('Не все удаляемые товары были найдены в заказе');
        }

        $totalOrderItemsCount = OrderItem::query()->where('order_id', $order->id)->where('is_deleted', false)->count();
        if ($totalOrderItemsCount == $orderItems->count()) {
            throw new ValidateException('Нельзя удалить последний товар в заказе');
        }

        DB::transaction(function () use ($orderItems, $order) {
            $shipments = [];
            foreach ($orderItems as $orderItem) {
                $orderItem->is_deleted = true;
                $orderItem->save();

                $shipments[$orderItem->shipment->id] = $orderItem->shipment;
            }

            $this->calcShipmentsAction->execute($shipments);

            $this->calcOrderPriceAction->execute($order->load('items'));
            $order->is_changed = true;
            $this->saveOrderAction->execute($order);
        });
    }
}
