<?php

namespace App\Domain\Common\Models\Tests\Factories;

use App\Domain\Common\Models\Setting;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class SettingFactory extends BaseModelFactory
{
    protected $model = Setting::class;

    public function definition()
    {
        return [
            'code' => $this->faker->unique()->randomEnum(SettingCodeEnum::cases()),
            'name' => $this->faker->text(50),
            'value' => $this->faker->numerify('##'),
        ];
    }
}
