<?php

namespace App\Domain\Common\Actions;

use App\Domain\Orders\Actions\Order\SaveOrderAction;
use App\Domain\Orders\Models\Order;
use App\Exceptions\IllegalOperationException;

class DeleteCustomerPersonalDataAction
{
    public function __construct(
        protected readonly CheckConditionsToDeleteCustomerPersonalDataAction $checkConditionsToDeleteCustomerPersonalDataAction,
        protected SaveOrderAction $saveOrderAction,
    ) {
    }

    public function execute(int $customerId): void
    {
        $customerPersonalData = $this->checkConditionsToDeleteCustomerPersonalDataAction->execute($customerId);

        if (!$customerPersonalData->canDelete) {
            throw new IllegalOperationException($customerPersonalData->message);
        }

        Order::whereCustomerId($customerId)
            ->eachById(function (Order $order) {
                $order->receiver_name = null;
                $order->receiver_phone = null;
                $order->receiver_email = null;
                $order->delivery_address = null;

                $this->saveOrderAction->execute($order);
            }, count: 50);
    }
}
