<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Actions\Data\CustomerPersonalData;
use App\Domain\Common\Models\Setting;
use App\Domain\Orders\Data\OrderStatus;
use App\Domain\Orders\Models\Order;
use App\Domain\Refunds\Data\RefundStatus;
use App\Http\ApiV1\OpenApiGenerated\Enums\SettingCodeEnum;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;

class CheckConditionsToDeleteCustomerPersonalDataAction
{
    protected string $message = '';

    public function execute(int $customerId): CustomerPersonalData
    {
        $customerPersonalData = new CustomerPersonalData();
        $customerPersonalData->canDelete = $this->checkConditionsToDelete($customerId);
        $customerPersonalData->message = $this->message;

        return $customerPersonalData;
    }

    protected function checkConditionsToDelete(int $customerId): bool
    {
        if ($this->getNotAvailableForDeleteOrdersCount($customerId) > 0) {
            $this->message = trans('customer_delete_messages.incomplete_orders');

            return false;
        }

        $minutesForRefundOrder = $this->getMinutesForRefundOrder();
        $refundStatusAt = Date::now()->subMinutes($minutesForRefundOrder);

        foreach ($this->loadCustomerDoneOrders($customerId) as $order) {
            if (!$this->checkForRefundConditions($order->refunds)) {
                $this->message = trans('customer_delete_messages.incomplete_refunds_in_order', ['orderId' => $order->id]);

                return false;
            }

            $orderStatusAt = $order->status_at;

            if (in_array($order->status, OrderStatus::done()) && $orderStatusAt > $refundStatusAt) {
                $this->message = trans('customer_delete_messages.incomplete_refund_time_in_order', ['orderId' => $order->id]);

                return false;
            }
        }

        return true;
    }

    protected function getMinutesForRefundOrder(): int
    {
        return (int)Setting::getValue(SettingCodeEnum::REFUND);
    }

    protected function checkForRefundConditions(?Collection $orderRefunds): bool
    {
        foreach ($orderRefunds as $refund) {
            if (!in_array($refund->status, RefundStatus::cancelled())) {
                return false;
            }
        }

        return true;
    }

    protected function getNotAvailableForDeleteOrdersCount(int $customerId): int
    {
        return Order::whereCustomerId($customerId)
            ->whereNotIn('status', OrderStatus::complete())
            ->count();
    }

    /**
     * @return Collection<Order>
     */
    protected function loadCustomerDoneOrders(int $customerId): Collection
    {
        return Order::with('refunds')
            ->whereCustomerId($customerId)
            ->whereIn('status', OrderStatus::complete())
            ->get();
    }
}
