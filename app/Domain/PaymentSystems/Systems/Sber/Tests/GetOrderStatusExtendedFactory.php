<?php

namespace App\Domain\PaymentSystems\Systems\Sber\Tests;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Voronkovich\SberbankAcquiring\OrderStatus;

class GetOrderStatusExtendedFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'errorCode' => 0,
            'orderStatus' => $this->faker->randomElement($this->statuses()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    private function statuses(): array
    {
        return [
            OrderStatus::CREATED,
            OrderStatus::APPROVED,
            OrderStatus::DEPOSITED,
            OrderStatus::REVERSED,
            OrderStatus::REFUNDED,
        ];
    }
}
