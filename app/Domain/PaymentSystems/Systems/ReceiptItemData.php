<?php

namespace App\Domain\PaymentSystems\Systems;

class ReceiptItemData
{
    public string $name;
    public $qty;
    public $price;
    public $pricePerOne;
}
