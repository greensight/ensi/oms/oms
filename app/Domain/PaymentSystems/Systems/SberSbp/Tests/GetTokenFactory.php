<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp\Tests;

use Ensi\LaravelTestFactories\BaseApiFactory;

class GetTokenFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'access_token' => $this->faker->uuid(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
