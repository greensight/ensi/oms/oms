<?php

namespace App\Domain\PaymentSystems\Systems\SberSbp\Tests;

use App\Domain\PaymentSystems\Data\SberSbp\CancelSberSbpOrderResponse;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOperationType;
use App\Domain\PaymentSystems\Data\SberSbp\SberSbpOrderStatus;
use App\Domain\PaymentSystems\Systems\SberSbp\SberSbpClient;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CancelSberSbpOrderFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->uuid(),
            'operation_id' => $this->faker->uuid(),
            'operation_type' => SberSbpOperationType::REFUND->value,
            'operation_currency' => SberSbpClient::CURRENCY_RUB,
            'operation_sum' => $this->faker->numberBetween(100, 1000),
            'operation_date_time' => $this->faker->date(SberSbpClient::DATE_TIME_FORMAT),
            'error_code' => '000000',
            'error_description' => $this->faker->text(20),
            'order_status' => SberSbpOrderStatus::REFUNDED->value,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponse(array $extra = []): CancelSberSbpOrderResponse
    {
        return new CancelSberSbpOrderResponse($this->makeArray($extra));
    }
}
