<?php

namespace App\Domain\PaymentSystems\Systems;

use App\Domain\PaymentSystems\Systems\Local\LocalPaymentData;
use App\Domain\PaymentSystems\Systems\Sber\SberPaymentData;
use App\Domain\PaymentSystems\Systems\SberSbp\SberSbpPaymentData;
use App\Domain\PaymentSystems\Systems\Sbp\SbpPaymentData;
use App\Domain\PaymentSystems\Systems\Tinkoff\TinkoffPaymentData;
use App\Domain\PaymentSystems\Systems\TinkoffSbp\TinkoffSbpPaymentData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;
use Carbon\CarbonInterface;

abstract class PaymentData
{
    public ?string $externalId;
    public ?string $paymentLink;
    public PaymentStatusEnum $status;
    public ?int $orderId;
    public ?int $customerId;
    public ?CarbonInterface $orderDate;
    public ?CarbonInterface $expiresAt;
    public ?CarbonInterface $payedAt;

    // Свойства, которые заполняются не всегда
    /** @var ReceiptItemData[] */
    public array $receiptItems = [];
    public string $customerEmail;
    public ?int $payedPrice;

    public function __construct(protected array $data, protected int $fullPrice, public int $deliveryPrice)
    {
    }

    public static function getClassBySystemId(PaymentSystemEnum $paymentSystemId): string
    {
        return match ($paymentSystemId) {
            PaymentSystemEnum::TEST => LocalPaymentData::class,
            PaymentSystemEnum::SBP => SbpPaymentData::class,
            PaymentSystemEnum::TINKOFF => TinkoffPaymentData::class,
            PaymentSystemEnum::TINKOFF_SBP => TinkoffSbpPaymentData::class,
            PaymentSystemEnum::SBER => SberPaymentData::class,
            PaymentSystemEnum::SBER_SBP => SberSbpPaymentData::class,
            //            PaymentSystemEnum::YANDEX => YandexPaymentData::class,
            default => throw new \Exception("Неизвестная система оплаты"),
        };
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getFullPrice(): int
    {
        return $this->fullPrice;
    }

    public function setStatusHold(): void
    {
        $this->setStatus(PaymentStatusEnum::HOLD, $this->fullPrice);
    }

    public function setStatusPaid(): void
    {
        if ($this->setStatus(PaymentStatusEnum::PAID, $this->fullPrice)) {
            $this->payedAt = now();
        }
    }

    public function setStatusCancelled(): void
    {
        $this->setStatus(PaymentStatusEnum::CANCELED, 0);
    }

    public function setStatusReturned(): void
    {
        $this->setStatus(PaymentStatusEnum::RETURNED, 0);
    }

    public function setStatusTimeout(): void
    {
        $this->setStatus(PaymentStatusEnum::TIMEOUT, 0);
    }

    protected function setStatus(PaymentStatusEnum $status, int $payedPrice): bool
    {
        if ($this->status == $status) {
            return false;
        }

        $this->status = $status;
        $this->payedPrice = $payedPrice;

        return true;
    }
}
