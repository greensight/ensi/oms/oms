<?php

namespace App\Domain\PaymentSystems\Systems\Tinkoff;

use App\Domain\Orders\Models\Order;
use App\Domain\PaymentSystems\Data\Tinkoff\CancelTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\ConfirmTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffLanguage;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentMethod;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentObject;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentStatus;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffPaymentType;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffTaxation;
use App\Domain\PaymentSystems\Data\Tinkoff\Enums\TinkoffTaxationType;
use App\Domain\PaymentSystems\Data\Tinkoff\GetStateTinkoffRequest;
use App\Domain\PaymentSystems\Data\Tinkoff\InitTinkoffRequest;
use App\Domain\PaymentSystems\Systems\PaymentSystem;
use App\Domain\PaymentSystems\Systems\ReceiptItemData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentStatusEnum;
use Exception;
use Psr\Log\LoggerInterface;

class TinkoffPaymentSystem extends PaymentSystem
{
    protected LoggerInterface $logger;

    private TinkoffClient $client;

    public function __construct()
    {
        $this->client = resolve(TinkoffClient::class);
        $this->logger = logger()->channel('payments_tinkoff');
    }

    /**
     * @param TinkoffPaymentData $paymentData
     * @throws Exception
     */
    public function createExternalPayment($paymentData, string $returnLink, ?string $failLink = null): void
    {
        $request = new InitTinkoffRequest();

        $request->amount = $paymentData->getFullPrice();
        $request->orderId = Order::makePaymentNumber($paymentData->orderId);
        $request->language = TinkoffLanguage::LANGUAGE_RU->value;
        $request->payType = TinkoffPaymentType::PAY_TYPE_TWO_STAGE->value;
        $request->successURL = $returnLink;
        $request->failURL = $failLink;
        $request->description = "Заказ №{$paymentData->orderId}";
        $request->receipt = [
            'Email' => $paymentData->customerEmail ?? null,
            'Taxation' => TinkoffTaxationType::TAX_TYPE_USN_INCOME,
            'Items' => $this->generateItems($paymentData->receiptItems, $paymentData->deliveryPrice),
        ];

        try {
            $this->logger->info('Регистрация нового заказа в Тинькофф');

            $response = $this->client->init($request);
            $this->logger->info('Ответ от Init', ['id' => $response->paymentId, 'status' => $response->status]);

        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе Init', ['message' => $e->getMessage()]);

            throw $e;
        }

        $paymentData->externalId = $response->paymentId;
        $paymentData->paymentLink = $response->paymentURL;
    }

    /**
     * @param TinkoffPaymentData $paymentData
     * @throws Exception
     */
    public function commitHoldedPayment($paymentData): void
    {
        $this->logger->info('Попытка списания захолдированных средств', [
            'payment_external_id' => $paymentData->externalId,
        ]);

        $request = new ConfirmTinkoffRequest();
        $request->paymentId = $paymentData->externalId;
        $request->amount = $paymentData->getFullPrice();

        try {
            $this->client->confirm($request);
            $paymentData->setStatusPaid();
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе Confirm', ['message' => $e->getMessage()]);

            throw $e;
        }

        $this->logger->info('Окончание списывания захолдированных средств', [
            'payment_external_id' => $paymentData->externalId,
        ]);
    }

    public function reinit($paymentData)
    {
    }

    /**
     * @param TinkoffPaymentData $paymentData
     * @throws Exception
     */
    public function revert($paymentData): void
    {
        $this->logger->info('Попытка отмены платежа', [
            'payment_external_id' => $paymentData->externalId,
        ]);

        $request = new CancelTinkoffRequest();
        $request->paymentId = $paymentData->externalId;

        try {
            $this->client->cancel($request);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе Cancel', ['message' => $e->getMessage()]);

            throw $e;
        }

        if ($paymentData->status == PaymentStatusEnum::HOLD) {
            $paymentData->setStatusCancelled();

            return;
        }

        if ($paymentData->status == PaymentStatusEnum::PAID) {
            $paymentData->setStatusReturned();

            return;
        }

        $paymentData->setStatusCancelled();
    }

    /**
     * @param TinkoffPaymentData $paymentData
     * @throws Exception
     */
    public function checkPayment($paymentData): void
    {
        if (!$paymentData->externalId) {
            $msg = 'Не указан payment_external_id';

            $this->logger->error($msg);

            throw new Exception($msg);
        }

        $request = new GetStateTinkoffRequest();
        $request->paymentId = $paymentData->externalId;

        try {
            $response = $this->client->getState($request);
        } catch (Exception $e) {
            $this->logger->error('Ошибка при запросе GetState', ['message' => $e->getMessage()]);

            throw $e;
        }

        $this->logger->info('Ответ GetState', ['id' => $response->paymentId, 'status' => $response->status]);

        if ($response->status == TinkoffPaymentStatus::STATUS_AUTHORIZED->value) {
            $paymentData->setStatusHold();
        }

        if ($response->status == TinkoffPaymentStatus::STATUS_CONFIRMED->value) {
            $paymentData->setStatusPaid();
        }

        if (in_array($response->status, [TinkoffPaymentStatus::STATUS_PARTIAL_REVERSED->value, TinkoffPaymentStatus::STATUS_REVERSED->value])) {
            $paymentData->setStatusCancelled();
        }

        if (in_array($response->status, [TinkoffPaymentStatus::STATUS_PARTIAL_REFUNDED->value, TinkoffPaymentStatus::STATUS_REFUNDED->value])) {
            $paymentData->setStatusReturned();
        }
    }

    /**
     * @param ReceiptItemData[] $receiptItems
     * @return array
     */
    protected function generateItems(array $receiptItems, int $deliveryPrice): array
    {
        $items = [];
        foreach ($receiptItems as $receiptItem) {
            $items[] = [
                'Name' => $receiptItem->name,
                'Quantity' => $receiptItem->qty,
                'Amount' => $receiptItem->price,
                'Price' => $receiptItem->pricePerOne,
                'PaymentMethod' => TinkoffPaymentMethod::FULL_PREPAYMENT,
                'PaymentObject' => TinkoffPaymentObject::COMMODITY,
                'Tax' => TinkoffTaxation::NONE,
            ];
        }

        if ($deliveryPrice > 0) {
            $items[] = [
                'Name' => 'Доставка',
                'Quantity' => 1,
                'Amount' => $deliveryPrice,
                'Price' => $deliveryPrice,
                'PaymentMethod' => TinkoffPaymentMethod::FULL_PREPAYMENT,
                'PaymentObject' => TinkoffPaymentObject::SERVICE,
                'Tax' => TinkoffTaxation::VAT20,
            ];
        }

        return $items;
    }
}
