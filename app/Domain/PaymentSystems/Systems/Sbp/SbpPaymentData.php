<?php

namespace App\Domain\PaymentSystems\Systems\Sbp;

use App\Domain\PaymentSystems\Systems\PaymentData;

class SbpPaymentData extends PaymentData
{
    public ?string $returnLink;
    public ?string $handlerUrl;
    public ?string $qrCode;

    public function __construct(array $data, protected int $fullPrice, public int $deliveryPrice)
    {
        parent::__construct($data, $fullPrice, $deliveryPrice);
        $this->returnLink = $data['returnLink'] ?? null;
        $this->handlerUrl = $data['handlerUrl'] ?? null;
        $this->qrCode = $data['qrCode'] ?? null;
    }

    public function getData(): array
    {
        return [
            'returnLink' => $this->returnLink,
            'handlerUrl' => $this->handlerUrl,
            'qrCode' => $this->qrCode,
        ];
    }
}
