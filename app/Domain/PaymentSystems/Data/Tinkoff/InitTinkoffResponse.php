<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class InitTinkoffResponse
{
    public string $orderId;
    public int $amount;
    public string $status;
    public string $paymentId;
    public string $paymentURL;

    public function __construct(array $response)
    {
        $this->orderId = $response['OrderId'];
        $this->amount = $response['Amount'];
        $this->status = $response['Status'];
        $this->paymentId = $response['PaymentId'];
        $this->paymentURL = $response['PaymentURL'];
    }
}
