<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff\Enums;

enum TinkoffTaxationType: string
{
    case TAX_TYPE_OSN = 'osn';
    case TAX_TYPE_USN_INCOME = 'usn_income';
    case TAX_TYPE_USN_INCOME_OUTCOME = 'usn_income_outcome';
    case TAX_TYPE_PATENT = 'patent';
    case TAX_TYPE_ENVD = 'envd';
    case TAX_TYPE_ESN = 'esn';
}
