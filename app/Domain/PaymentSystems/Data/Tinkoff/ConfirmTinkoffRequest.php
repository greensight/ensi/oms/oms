<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class ConfirmTinkoffRequest implements TinkoffRequest
{
    public string $paymentId;
    public int $amount;

    public function toArray(): array
    {
        return [
            'PaymentId' => $this->paymentId,
            'Amount' => $this->amount,
        ];
    }
}
