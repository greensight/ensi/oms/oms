<?php

namespace App\Domain\PaymentSystems\Data\Tinkoff;

class GetQrStateTinkoffResponse extends AbstractTinkoffResponse
{
    public string $orderId;
    public string $status;
    public int $amount;

    public function __construct(array $response)
    {
        parent::__construct($response);

        $this->orderId = $response['OrderId'];
        $this->status = $response['Status'];
        $this->amount = $response['Amount'];
    }
}
