<?php

namespace App\Domain\PaymentSystems\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaymentSystemEnum;

class PaymentSystem
{
    public string $name;

    public function __construct(public PaymentSystemEnum $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById(): void
    {
        $this->name = match ($this->id) {
            //            PaymentSystemEnum::YANDEX => 'Яндекс',
            PaymentSystemEnum::TEST => 'Тест',
            PaymentSystemEnum::CASH => 'Наличные',
            PaymentSystemEnum::SBP => 'СБП',
            PaymentSystemEnum::TINKOFF => 'Тинькофф онлайн',
            PaymentSystemEnum::TINKOFF_SBP => 'Тинькофф СБП',
            PaymentSystemEnum::SBER => 'Сбер',
            PaymentSystemEnum::SBER_SBP => 'Сбербанк СБП'
        };
    }

    /**
     * @return static[]
     */
    public static function all(): array
    {
        $all = [];

        foreach (PaymentSystemEnum::cases() as $system) {
            $all[] = new static($system);
        }

        return $all;
    }

    public static function editable(): array
    {
        return [
            PaymentSystemEnum::SBER,
            PaymentSystemEnum::TINKOFF,
        ];
    }

    public static function isSbp(PaymentSystemEnum $paymentSystemId): bool
    {
        return in_array($paymentSystemId, static::sbpPaymentSystems());
    }

    public static function sbpPaymentSystems(): array
    {
        return [PaymentSystemEnum::SBP, PaymentSystemEnum::TINKOFF_SBP, PaymentSystemEnum::SBER_SBP];
    }
}
