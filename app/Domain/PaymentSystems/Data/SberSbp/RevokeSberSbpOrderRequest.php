<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class RevokeSberSbpOrderRequest implements SberSbpRequest
{
    public string $orderId;

    public function toArray(): array
    {
        return [
            'order_id' => $this->orderId,
        ];
    }
}
