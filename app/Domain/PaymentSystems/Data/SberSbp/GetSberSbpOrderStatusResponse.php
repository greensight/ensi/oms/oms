<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

use Illuminate\Support\Collection;

class GetSberSbpOrderStatusResponse
{
    public ?string $idQr;
    public ?string $tid;
    public string $errorCode;
    public ?string $errorDescription;
    public ?string $orderId;
    public ?string $orderState;
    /** @var Collection|OrderOperationParams[] */
    public Collection $orderOperationParams;

    public function __construct(array $response)
    {
        $this->idQr = $response['id_qr'] ?? null;
        $this->tid = $response['tid'] ?? null;
        $this->errorCode = $response['error_code'];
        $this->errorDescription = $response['error_description'] ?? null;
        $this->orderId = $response['order_id'] ?? null;
        $this->orderState = $response['order_state'] ?? null;

        $params = collect();

        foreach ($response['order_operation_params'] ?? [] as $operationParams) {
            $params->push(new OrderOperationParams($operationParams));
        }

        $this->orderOperationParams = $params;
    }

    public function isValid(): bool
    {
        return filled($this->orderState);
    }
}
