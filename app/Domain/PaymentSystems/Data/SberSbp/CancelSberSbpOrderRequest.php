<?php

namespace App\Domain\PaymentSystems\Data\SberSbp;

class CancelSberSbpOrderRequest implements SberSbpRequest
{
    public string $orderId;
    public string $operationId;
    public SberSbpOperationType $operationType;
    public int $cancelOperationSum;
    public string $operationCurrency;
    public string $authCode;
    public string $idQr;
    public string $tid;

    public function toArray(): array
    {
        return [
            'order_id' => $this->orderId,
            'operation_id' => $this->operationId,
            'operation_type' => $this->operationType->value,
            'cancel_operation_sum' => $this->cancelOperationSum,
            'operation_currency' => $this->operationCurrency,
            'auth_code' => $this->authCode,
            'id_qr' => $this->idQr,
            'tid' => $this->tid,
        ];
    }
}
