<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\OrderPayload;
use App\Domain\Orders\Models\Order;

class SendOrderEventAction extends SendMessageAction
{
    public function execute(Order $order, string $event): void
    {
        $modelEvent = new ModelEventMessage($order, new OrderPayload($order), $event, 'orders');
        $this->send($modelEvent);
    }
}
