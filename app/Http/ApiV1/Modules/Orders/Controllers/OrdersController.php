<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Order\AddOrderCommentAction;
use App\Domain\Orders\Actions\Order\ChangeOrderDeliveryAction;
use App\Domain\Orders\Actions\Order\ChangeOrderPaymentSystemAction;
use App\Domain\Orders\Actions\Order\CheckOrderPaymentStatusAction;
use App\Domain\Orders\Actions\Order\PatchOrderAction;
use App\Domain\Orders\Actions\Payment\PaymentSystem\StartPaymentAction;
use App\Domain\Orders\Actions\UpsertOrders\CommitOrder\CommitOrderAction;
use App\Exceptions\ValidateException;
use App\Http\ApiV1\Modules\Orders\Queries\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\AddOrderCommentRequest;
use App\Http\ApiV1\Modules\Orders\Requests\ChangeOrderDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Requests\ChangeOrderPaymentSystemRequest;
use App\Http\ApiV1\Modules\Orders\Requests\CommitOrderRequest;
use App\Http\ApiV1\Modules\Orders\Requests\PatchOrderRequest;
use App\Http\ApiV1\Modules\Orders\Requests\StartOrderPaymentRequest;
use App\Http\ApiV1\Modules\Orders\Resources\CommitOrderResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrdersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\DataResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\OffersClient\ApiException as OffersApiException;
use Ensi\PimClient\ApiException as PimApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OrdersController
{
    public function search(PageBuilderFactory $pageBuilderFactory, OrdersQuery $query): AnonymousResourceCollection
    {
        return OrdersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, OrdersQuery $query): OrdersResource
    {
        return new OrdersResource($query->findOrFail($id));
    }

    /**
     * @throws ValidateException
     */
    public function patch(int $id, PatchOrderRequest $request, PatchOrderAction $action): OrdersResource
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    /**
     * @throws ValidateException
     */
    public function changeDelivery(
        int $id,
        ChangeOrderDeliveryRequest $request,
        ChangeOrderDeliveryAction $action
    ): OrdersResource {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function paymentCheck(int $id, CheckOrderPaymentStatusAction $action): DataResource
    {
        return new DataResource([
            'payment_status' => $action->execute($id),
        ]);
    }

    /**
     * @throws ValidateException
     */
    public function paymentStart(
        int $id,
        StartPaymentAction $action,
        StartOrderPaymentRequest $request,
        OrdersQuery $ordersQuery
    ): DataResource {
        $link = $action->execute($ordersQuery->findOrFail($id), $request->getReturnUrl());

        return new DataResource([
            'payment_link' => $link,
        ]);
    }

    /**
     * @throws ValidateException
     */
    public function changePaymentSystem(
        int $id,
        ChangeOrderPaymentSystemAction $action,
        ChangeOrderPaymentSystemRequest $request,
        OrdersQuery $ordersQuery
    ): OrdersResource {
        return new OrdersResource($action->execute($ordersQuery->findOrFail($id), $request->getPaymentSystem()));
    }

    public function addComment(int $id, AddOrderCommentRequest $request, AddOrderCommentAction $action): EmptyResource
    {
        $action->execute($id, $request->getText());

        return new EmptyResource();
    }

    /**
     * @throws ValidateException
     * @throws OffersApiException
     * @throws PimApiException
     */
    public function commit(CommitOrderRequest $request, CommitOrderAction $action): CommitOrderResource
    {
        return CommitOrderResource::make($action->execute($request->convertToObject()));
    }
}
