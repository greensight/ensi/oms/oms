<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Payment\PaymentSystem\HandlePaymentAction;
use App\Domain\PaymentSystems\Systems\Local\LocalPushPaymentData;
use Illuminate\Http\Request;

class PaymentsController
{
    public function handlerLocal(Request $request, HandlePaymentAction $action)
    {
        $action->execute(new LocalPushPaymentData($request->all()));

        return response('ok');
    }
}
