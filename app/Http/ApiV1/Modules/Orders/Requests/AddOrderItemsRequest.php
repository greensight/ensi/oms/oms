<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Domain\Orders\Actions\UpsertOrders\Data\OrderItemData;
use Illuminate\Foundation\Http\FormRequest;

class AddOrderItemsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'order_items' => ['required', 'array'],
            'order_items.*.offer_id' => ['required', 'integer', 'min:1'],
            'order_items.*.qty' => ['required', 'numeric', 'min:0'],
        ];
    }

    /**
     * @return OrderItemData[]
     */
    public function getOrderItems(): array
    {
        $orderItems = [];
        foreach ($this->validated('order_items') as $item) {
            $orderItem = new OrderItemData();
            $orderItem->offerId = $item['offer_id'];
            $orderItem->qty = $item['qty'];

            $orderItems[] = $orderItem;
        }

        return $orderItems;
    }
}
