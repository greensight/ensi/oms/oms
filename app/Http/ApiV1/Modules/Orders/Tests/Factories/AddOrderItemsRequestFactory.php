<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class AddOrderItemsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_items' => $this->faker->randomList(fn () => AddOrderItemsRequestItemFactory::new()->make(), 1, 3),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
