<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Models\Shipment;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Shipment
 */
class ShipmentsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'delivery_id' => $this->delivery_id,
            'store_id' => $this->store_id,

            'number' => $this->number,

            'status' => $this->status,
            'status_at' => $this->status_at,

            'cost' => $this->cost,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'weight' => $this->weight,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'delivery' => new DeliveriesResource($this->whenLoaded('delivery')),
            'order_items' => OrderItemsResource::collection($this->whenLoaded('orderItems')),
        ];
    }
}
