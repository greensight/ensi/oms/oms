<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateRefundReasonRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'code' => ['required', 'string', 'max:20'],
            'name' => ['required', 'string', 'max:255'],
            'description' => ['string', 'nullable'],
        ];
    }
}
