<?php

namespace App\Http\ApiV1\Modules\Refunds\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttachRefundFileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:2048'],
        ];
    }
}
