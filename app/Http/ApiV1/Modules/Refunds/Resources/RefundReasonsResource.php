<?php

namespace App\Http\ApiV1\Modules\Refunds\Resources;

use App\Domain\Refunds\Models\RefundReason;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin RefundReason */
class RefundReasonsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
