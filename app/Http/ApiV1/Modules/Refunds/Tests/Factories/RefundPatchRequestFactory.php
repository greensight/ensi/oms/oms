<?php

namespace App\Http\ApiV1\Modules\Refunds\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\RefundStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Database\Eloquent\Collection;

class RefundPatchRequestFactory extends BaseApiFactory
{
    public int $orderId;
    public Collection $orderItems;
    public Collection $reasons;
    public bool $fullRefund;
    public int|null $status;

    protected function definition(): array
    {
        return [
            'status' => $this->faker->randomEnum(RefundStatusEnum::cases()),
            'responsible_id' => $this->faker->modelId(),
            'rejection_comment' => $this->faker->text(100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
