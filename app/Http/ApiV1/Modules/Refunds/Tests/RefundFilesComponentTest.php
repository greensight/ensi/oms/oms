<?php

use App\Domain\Refunds\Models\Refund;
use App\Domain\Refunds\Models\RefundFile;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\post;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/refunds/refunds/{id}:attach-file success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);

    $refund = Refund::factory()->create();
    $id = $refund->id;

    $requestBody = ['file' => UploadedFile::fake()->create("refund.doc", kilobytes: 100)];

    $response = post("/api/v1/refunds/refunds/$id:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"]);

    $response->assertStatus(201);
    $responseFile = $response->decodeResponseJson()['data'];

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertExists($responseFile['file']['path']);

    expect(Refund::query()->where('id', $id)->whereHas('files')->exists())->toBeTrue();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refunds/{id}:attach-file fail if size exceeds 2mb', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $refund = Refund::factory()->create();
    $id = $refund->id;

    $requestBody = ['file' => UploadedFile::fake()->create("refund.doc", kilobytes: 3000)];

    post("/api/v1/refunds/refunds/$id:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");

    expect(Refund::query()->where('id', $id)->whereHas('files')->exists())->toBeFalse();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/refunds/refunds/{id}:attach-file 404 if refund not found', function () {
    $requestBody = ['file' => UploadedFile::fake()->create("refund.doc", kilobytes: 100)];

    post("/api/v1/refunds/refunds/12345:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(404)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/refunds/refunds/{id}:delete-files success', function (bool $fileExists, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $filePath = "refunds/file.jpg";
    $diskName = resolve(EnsiFilesystemManager::class)->protectedDiskName();
    Storage::fake($diskName);
    if ($fileExists) {
        Storage::disk($diskName)->put($filePath, 'some content');
    }
    /** @var Refund $refund */
    $refund = Refund::factory()->create();
    /** @var RefundFile $refundFile */
    $refundFile = RefundFile::factory()->for($refund)->withPath($filePath)->create();

    deleteJson("/api/v1/refunds/refunds/{$refund->id}:delete-files", ['file_ids' => [$refundFile->id]])
        ->assertStatus(200)
        ->assertJsonPath('data', null);

    /** @var \Illuminate\Filesystem\FilesystemAdapter */
    $disk = Storage::disk($diskName);
    $disk->assertMissing($filePath);
    assertDatabaseMissing((new RefundFile())->getTable(), [
        'refund_id' => $refund->id,
    ]);
})->with(
    [true, false],
    FakerProvider::$optionalDataset
);
