<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneRule implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match("/^\+7[0-9]{10}$/", $value);
    }

    public function message()
    {
        return 'Поле :attribute должно соответствовать формату +79998887766';
    }
}
