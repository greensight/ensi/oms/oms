<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->decimal('product_weight', 18, 4)->nullable(); // not null
            $table->decimal('product_weight_gross', 18, 4)->nullable(); // not null
            $table->decimal('product_width', 18, 4)->nullable(); // not null
            $table->decimal('product_height', 18, 4)->nullable(); // not null
            $table->decimal('product_length', 18, 4)->nullable(); // not null
            $table->string('product_storage_area')->nullable();
            $table->string('product_barcode')->nullable();
            $table->unsignedBigInteger('offer_store_id')->nullable(); // not null
            $table->string('offer_external_id')->nullable(); // not null
            $table->string('offer_storage_address')->nullable();
        });

        $items = DB::table('basket_items')->get();
        foreach ($items as $item) {
            $product = json_decode($item->product, true);
            DB::table('basket_items')->where([
                'id' => $item->id,
            ])->update([
                'product_weight' => $product['weight'],
                'product_weight_gross' => $product['weight_gross'],
                'product_width' => $product['width'],
                'product_height' => $product['height'],
                'product_length' => $product['length'],
                'product_storage_area' => $product['storage_area'],
                'product_barcode' => $product['barcode'],
                'offer_store_id' => $product['store_id'],
                'offer_external_id' => $product['external_id'],
                'offer_storage_address' => $product['storage_address'],
            ]);
        }

        Schema::table('basket_items', function (Blueprint $table) {
            $table->decimal('product_weight', 18, 4)->nullable(false)->change();
            $table->decimal('product_weight_gross', 18, 4)->nullable(false)->change();
            $table->decimal('product_width', 18, 4)->nullable(false)->change();
            $table->decimal('product_height', 18, 4)->nullable(false)->change();
            $table->decimal('product_length', 18, 4)->nullable(false)->change();
            $table->unsignedBigInteger('offer_store_id')->nullable(false)->change();
            $table->string('offer_external_id')->nullable(false)->change();
            $table->dropColumn('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->dropColumn('product_weight');
            $table->dropColumn('product_weight_gross');
            $table->dropColumn('product_width');
            $table->dropColumn('product_height');
            $table->dropColumn('product_length');
            $table->dropColumn('product_storage_area');
            $table->dropColumn('product_barcode');
            $table->dropColumn('offer_store_id');
            $table->dropColumn('offer_external_id');
            $table->dropColumn('offer_storage_address');
            $table->json('product');
        });
    }
};
