<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('status_external_id');
            $table->dropColumn('external_id');
            $table->dropColumn('status_external_id_at');
            $table->dropColumn('error_external_id');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('manager_comment', 'client_comment');
            $table->dropColumn('certificates');
            $table->string('promo_code')->nullable();
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('offer_id')->nullable(false)->change();
            $table->dropColumn('offer_store_id');
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('delivery_service_zero_mile');
            $table->dropColumn('required_shipping_at');
        });
        Schema::dropIfExists('payment_options');
        Schema::dropIfExists('order_discounts');
        Schema::dropIfExists('order_promo_codes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('order_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->json('discounts')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
        Schema::create('order_promo_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('promo_code_id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->unsignedTinyInteger('type');
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->unsignedBigInteger('gift_id')->nullable();
            $table->unsignedBigInteger('bonus_id')->nullable();
            $table->boolean('is_personal');

            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
        Schema::create('payment_options', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('seller_id')->unsigned();
            $table->string('terminal_id');
            $table->timestamps();
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('offer_store_id')->nullable();
        });
        DB::table('order_items')->whereNull('offer_store_id')->update(['offer_store_id' => 1]);
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('offer_id')->nullable(true)->change();
            $table->unsignedBigInteger('offer_store_id')->nullable(false)->change();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->json('certificates')->nullable();
            $table->renameColumn('client_comment', 'manager_comment');
            $table->dropColumn('promo_code');
        });
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('status_external_id')->nullable();
            $table->string('external_id')->nullable();
            $table->text('error_external_id')->nullable();
            $table->dateTime('status_external_id_at')->nullable();
        });
        Schema::table('shipments', function (Blueprint $table) {
            $table->timestamp('required_shipping_at')->nullable();
        });
        DB::table('shipments')->whereNull('required_shipping_at')->update(['required_shipping_at' => now()]);
        Schema::table('shipments', function (Blueprint $table) {
            $table->unsignedTinyInteger('delivery_service_zero_mile')->nullable();
        });
        DB::statement('alter table shipments alter column required_shipping_at set not null');
    }
};
