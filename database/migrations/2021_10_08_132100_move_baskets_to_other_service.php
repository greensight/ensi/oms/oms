<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('delete from basket_items USING baskets WHERE baskets.id = basket_items.basket_id and baskets.is_belongs_to_order = false');
        DB::statement('delete from baskets WHERE is_belongs_to_order = false');

        Schema::rename('basket_items', 'order_items');
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('referrer_id');
            $table->dropColumn('type');
            $table->decimal('price', 18, 4)->nullable(false)->change();
            $table->decimal('cost', 18, 4)->nullable(false)->change();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('shipment_id')->nullable();
        });

        $basketItems = DB::table('order_items')
            ->join('baskets', 'baskets.id', '=', 'order_items.basket_id')
            ->join('orders', 'orders.basket_id', '=', 'baskets.id')
            ->join('shipment_items', 'shipment_items.basket_item_id', '=', 'order_items.id')
            ->selectRaw('order_items.id, orders.id order_id, shipment_items.shipment_id as shipment_id')
            ->get();

        foreach ($basketItems as $basketItem) {
            DB::table('order_items')->where('id', $basketItem->id)->update([
                'order_id' => $basketItem->order_id,
                'shipment_id' => $basketItem->shipment_id,
            ]);
        }

        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('basket_id');
            $table->unsignedBigInteger('order_id')->nullable(false)->change();
            $table->unsignedBigInteger('shipment_id')->nullable(false)->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('basket_id');
            $table->dropColumn('seller_id');
        });

        Schema::dropIfExists('baskets');
        Schema::dropIfExists('shipment_items');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('order_items', 'basket_items');
        Schema::create('shipment_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shipment_id');
            $table->unsignedBigInteger('basket_item_id');

            $table->timestamps();

            $table->foreign('shipment_id')->references('id')->on('shipments');
            $table->foreign('basket_item_id')->references('id')->on('basket_items');

            $table->unique(['shipment_id', 'basket_item_id']);
        });
        Schema::create('baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('session_id', 255)->nullable();
            $table->unsignedInteger('type');
            $table->boolean('is_belongs_to_order')->default(false);

            $table->timestamps();
            $table->index(['updated_at']);
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('basket_id')->nullable();
            $table->unsignedBigInteger('seller_id')->nullable();
        });

        $orders = DB::table('orders')->get();
        foreach ($orders as $order) {
            $basketId = DB::table('baskets')->insertGetId([
                'seller_id' => 1,
                'customer_id' => $order->customer_id,
                'type' => 1,
                'is_belongs_to_order' => true,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('orders')->where('id', $order->id)->update([
                'basket_id' => $basketId,
                'seller_id' => 1,
            ]);
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('basket_id')->nullable(false)->unique()->change();
            $table->unsignedBigInteger('seller_id')->nullable(false)->change();

            $table->foreign('basket_id')->on('baskets')->references('id');
        });

        Schema::table('basket_items', function (Blueprint $table) {
            $table->bigInteger('referrer_id')->nullable();
            $table->integer('type')->nullable();
            $table->bigInteger('basket_id')->nullable();
            $table->decimal('price', 18, 4)->nullable()->change();
            $table->decimal('cost', 18, 4)->default(0.0)->change();
        });

        $basketItems = DB::table('basket_items')
            ->join('orders', 'orders.id', '=', 'basket_items.order_id')
            ->selectRaw('basket_items.id, basket_items.shipment_id, orders.basket_id basket_id')
            ->get();

        foreach ($basketItems as $basketItem) {
            DB::table('basket_items')->where('id', $basketItem->id)->update([
                'type' => 1,
                'basket_id' => $basketItem->basket_id,
            ]);

            DB::table('shipment_items')->insert([
                'shipment_id' => $basketItem->shipment_id,
                'basket_item_id' => $basketItem->id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        Schema::table('basket_items', function (Blueprint $table) {
            $table->integer('type')->nullable(false)->change();
            $table->bigInteger('basket_id')->nullable(false)->change();
            $table->dropColumn('order_id');
            $table->dropColumn('shipment_id');

            $table->foreign('basket_id')->on('baskets')->references('id');
        });
    }
};
