<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('payed_at')->nullable();
            $table->unsignedSmallInteger('payment_system')->nullable();
            $table->unsignedSmallInteger('payment_method')->nullable();
            $table->dateTime('payment_expires_at')->nullable();
            $table->json('payment_data')->nullable();
            $table->string('payment_link')->nullable();
            $table->string('payment_external_id')->nullable();
        });
        DB::statement('update orders set
            payed_at = payments.payed_at,
            payment_system = payments.payment_system,
            payment_expires_at = payments.expires_at,
            payment_data = payments.data,
            payment_link = payments.payment_link,
            payment_external_id = payments.external_id,
            payment_method = payments.payment_method
        from payments where payments.order_id = orders.id');

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedSmallInteger('payment_system')->nullable(false)->change();
            $table->unsignedSmallInteger('payment_method')->nullable(false)->change();
            $table->json('payment_data')->nullable(false)->change();
        });

        Schema::dropIfExists('payments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedTinyInteger('payment_system');
            $table->unsignedTinyInteger('status')->default(1);

            $table->float('sum');
            $table->tinyInteger('payment_method', false, true);
            $table->dateTime('payed_at')->nullable();
            $table->dateTime('expires_at')->nullable();

            $table->json('data');
            $table->string('payment_link')->nullable();
            $table->string('external_id')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
        });

        $orders = DB::table('orders')->get();
        foreach ($orders as $order) {
            DB::table('payments')->insert([
                'order_id' => $order->id,
                'payment_system' => $order->payment_system,
                'status' => $order->payment_status,
                'sum' => $order->price,
                'payment_method' => $order->payment_method,
                'payed_at' => $order->payed_at,
                'expires_at' => $order->payment_expires_at,
                'data' => $order->payment_data,
                'payment_link' => $order->payment_link,
                'external_id' => $order->payment_external_id,
                'created_at' => $order->created_at,
                'updated_at' => $order->created_at,
            ]);
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payed_at');
            $table->dropColumn('payment_system');
            $table->dropColumn('payment_method');
            $table->dropColumn('payment_expires_at');
            $table->dropColumn('payment_data');
            $table->dropColumn('payment_link');
            $table->dropColumn('payment_external_id');
        });
    }
};
