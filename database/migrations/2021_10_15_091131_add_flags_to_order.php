<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('is_expired')->default(false);
            $table->timestamp('is_expired_at', 6)->nullable();
            $table->boolean('is_return')->default(false);
            $table->timestamp('is_return_at', 6)->nullable();

            $table->boolean('is_partial_return')->default(false);
            $table->timestamp('is_partial_return_at', 6)->nullable();

            $table->text('problem_comment')->nullable();
        });

        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('is_problem');
            $table->dropColumn('is_problem_at');
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('is_problem');
            $table->dropColumn('is_problem_at');
            $table->dropColumn('assembly_problem_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('is_expired');
            $table->dropColumn('is_expired_at');

            $table->dropColumn('is_return');
            $table->dropColumn('is_return_at');

            $table->dropColumn('is_partial_return');
            $table->dropColumn('is_partial_return_at');

            $table->dropColumn('problem_comment');
        });

        Schema::table('deliveries', function (Blueprint $table) {
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();
        });

        Schema::table('shipments', function (Blueprint $table) {
            $table->boolean('is_problem')->default(false);
            $table->dateTime('is_problem_at')->nullable();
            $table->text('assembly_problem_comment')->nullable();
        });
    }
};
