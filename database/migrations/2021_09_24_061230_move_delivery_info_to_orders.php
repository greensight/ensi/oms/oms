<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_method')->nullable();
            $table->unsignedSmallInteger('delivery_service')->nullable();
            $table->unsignedBigInteger('delivery_tariff_id')->nullable();
            $table->unsignedBigInteger('delivery_point_id')->nullable();
            $table->json('delivery_address')->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('receiver_phone')->nullable();
            $table->string('receiver_email')->nullable();
            $table->dropColumn('delivery_type');
        });

        DB::statement('update orders set
            delivery_method = deliveries.delivery_method, 
            delivery_service = deliveries.delivery_service, 
            delivery_tariff_id = deliveries.tariff_id, 
            delivery_point_id = deliveries.point_id, 
            delivery_address = deliveries.delivery_address, 
            receiver_name = deliveries.receiver_name, 
            receiver_phone = deliveries.receiver_phone,
            receiver_email = deliveries.receiver_email
        from deliveries where deliveries.order_id = orders.id');
        DB::statement('update orders set receiver_name = \'Имя Фамилия Отчество\' where receiver_name is null');
        DB::statement('update orders set receiver_phone = \'+79998887766\' where receiver_phone is null');
        DB::statement('update orders set receiver_email = \'email@domain.ru\' where receiver_email is null');

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_method')->nullable(false)->change();
            $table->unsignedSmallInteger('delivery_service')->nullable(false)->change();
            $table->unsignedBigInteger('delivery_tariff_id')->nullable(false)->change();
            $table->string('receiver_name')->nullable(false)->change();
            $table->string('receiver_phone')->nullable(false)->change();
            $table->string('receiver_email')->nullable(false)->change();
        });

        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('delivery_method');
            $table->dropColumn('delivery_service');
            $table->dropColumn('tariff_id');
            $table->dropColumn('point_id');
            $table->dropColumn('delivery_address');
            $table->dropColumn('receiver_name');
            $table->dropColumn('receiver_phone');
            $table->dropColumn('receiver_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_method')->nullable();
            $table->unsignedSmallInteger('delivery_service')->nullable();
            $table->integer('tariff_id')->nullable();
            $table->integer('point_id')->nullable();
            $table->json('delivery_address')->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('receiver_phone')->nullable();
            $table->string('receiver_email')->nullable();
            $table->index(['receiver_name', 'receiver_phone', 'receiver_email'], 'delivery_receiver_name_receiver_phone_receiver_email_index');
        });

        DB::statement('update deliveries set
            delivery_method = orders.delivery_method, 
            delivery_service = orders.delivery_service, 
            tariff_id = orders.delivery_tariff_id, 
            point_id = orders.delivery_point_id, 
            delivery_address = orders.delivery_address, 
            receiver_name = orders.receiver_name, 
            receiver_phone = orders.receiver_phone, 
            receiver_email = orders.receiver_email
        from orders where deliveries.order_id = orders.id');

        Schema::table('deliveries', function (Blueprint $table) {
            $table->unsignedBigInteger('delivery_method')->nullable(false)->change();
            $table->unsignedSmallInteger('delivery_service')->nullable(false)->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_method');
            $table->dropColumn('delivery_service');
            $table->dropColumn('delivery_tariff_id');
            $table->dropColumn('delivery_point_id');
            $table->dropColumn('delivery_address');
            $table->dropColumn('receiver_name');
            $table->dropColumn('receiver_phone');
            $table->dropColumn('receiver_email');
            $table->unsignedSmallInteger('delivery_type')->nullable();
        });

        DB::statement('update orders set delivery_type = 1');

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedSmallInteger('delivery_type')->nullable(false)->change();
        });
    }
};
