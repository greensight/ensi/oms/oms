<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'sber' => [
        'login' => env('SBER_LOGIN', ''),
        'pass' => env('SBER_PASS', ''),

        'qr' => [
            'client_id' => env('SBER_QR_CLIENT_ID', ''),
            'client_secret' => env('SBER_QR_CLIENT_SECRET', ''),
            'cert_path' => env('SBER_CERT_PATH', ''),
            'base_uri' => env('SBER_QR_BASE_URI', ''),
            'oauth_url' => env('SBER_QR_OAUTH_URL', ''),
            'scope' => [
                'create' => env('SBER_QR_SCOPE_CREATE', ''),
                'status' => env('SBER_QR_SCOPE_STATUS', ''),
                'revoke' => env('SBER_QR_SCOPE_REVOKE', ''),
                'cancel' => env('SBER_QR_SCOPE_CANCEL', ''),
            ],
            'id_qr' => env('SBER_QR_ID_QR', ''),
            'terminal_id' => env('SBER_QR_TERMINAL_ID', ''),
        ],
    ],

    'tinkoff' => [
        'terminal_key' => env('TINKOFF_TERMINAL_KEY'),
        'secret_key' => env('TINKOFF_SECRET_KEY'),
        'base_url' => env('TINKOFF_BASE_URI'),
    ],
];
